from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

def index(request):
	if request.user.is_authenticated:
		return render(request, 'app/home.html', context={'connected': True,'user':request.user.username})
	return render(request, 'app/home.html', context={'connected': False})

@login_required
def profile(request):
	if request.user.is_authenticated:
		return render(request, 'app/profile.html', context={'connected': True,'user':request.user.username})
	return render(request, 'app/profile.html', context={'connected': False})
